# Nextcloud SaxID-API-Integration

Diese Nextcloud-App ermöglicht das automatische Hinzufügen von Benutzern beim
ersten Login zur SaxID-API.

## Konfiguration

Folgende Einträge müssen zur `config/config.php` hinzugefügt werden:

* `saxid.api_url`: Basis-URL zur verwendeten SaxID-Instanz
* `saxid.api_token`: Zugriffstoken für die SaxID-API
* `saxid.sp`: Service Provider auf der SaxID-Instanz
* `saxid.setup_token`: Setup-Token der SaxID

### Beispiel

```php
  // ...
  'saxid.api_url' => 'http://localhost:8000/res/',
  'saxid.api_token' => '83f094a5011d9c2bddb684cd541de8534ceb09ba',
  'saxid.sp' => '892041602a9347df822a0d8fed0b79f2',
  'saxid.setup_token' => '53565644',
  // ...
```
