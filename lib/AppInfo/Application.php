<?php

/**
 * Owncloud - SaxID API Integration
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Benedikt Geißler <geib@hrz.tu-chemnitz.de>
 */

namespace OCA\SaxIdIntegration\AppInfo;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\Util;
use OCP\IUser;

class Application extends App implements IBootstrap {

    public function __construct() {
        parent::__construct('saxidintegration');
    }

	public function register(IRegistrationContext $context): void {
		// TODO: Implement register() method.
	}

	public function boot(IBootContext $context): void {
		Util::writeLog("saxidapiintegration", "registerHooksAndListeners", Util::INFO);

		$dispatcher = $context->getServerContainer()->getEventDispatcher();

		// first time login event setup
		$dispatcher->addListener(IUser::class . '::firstLogin', function ($event) {
			Util::writeLog("saxidapiintegration", "Handling firstLogin event.", Util::INFO);
			$user = $event->getSubject();
			Application::callSaxIdApi($event->getSubject($user));

		});
	}


	/**
     * @internal
     */
    public static function callSaxIdApi(IUser $user) {
        $uid = $user->getUID();

        Util::writeLog("saxidapiintegration", sprintf('callSaxIdApi(%s)', $uid), Util::INFO);

        $config = \OC::$server->getConfig();
        $apiurl = $config->getSystemValue('saxid.api_url');
        $apitoken = $config->getSystemValue('saxid.api_token');
        $username = $uid;
        $userid = $uid;
        $format = 'Y-m-d\TH:i:s\Z';
        $sp = $config->getSystemValue('saxid.sp');
        $setuptoken = $config->getSystemValue('saxid.setup_token');
        $header = sprintf("Content-Type: application/json\r\nAuthorization: Token %s\r\n", $apitoken);
        $attributes = [
            ['name' => 'quota', 'value' => 0],
            ['name' => 'quota_used', 'value' => 0],
            ['name' => 'quota_free', 'value' => 0],
        ];
        $data = [
            'eppn' => $username,
            'sp_primary_key' => $userid,
            'expiry_date' => date($format,
                    mktime(0, 0, 0, date('m'), date('d') + 7)),
            'deletion_date' => date($format,
                    mktime(0, 0, 0, date('m'), date('d') + 7 + 30)),
            'sp' => $sp,
            'setup_token' => $setuptoken,
            'attributes' => $attributes,
        ];
        $options = [
            'http' => [
                'header' => $header,
                'method' => 'POST',
                'content' => json_encode($data),
            ],
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($apiurl, false, $context);
        Util::writeLog('saxidapiintegration', sprintf('POST result: %s', $result), Util::INFO);
    }
}
